# sdl_linking

test linking with sdl


```sh
$ cd build
$ cmake ../ -DSHARED_LINKING=true

```

TODO: static link, dbg

```sh

$ ldd sdl-dynamic-linking 
	linux-vdso.so.1 (0x00007fff89350000)
	libSDL2-2.0d.so.0 => /home/vito/eclipseCosmos/cppOpenGlws/sdl_link/sdl_linking/libs/libSDL2-2.0d.so.0 (0x00007f69ff9ca000)
	libstdc++.so.6 => /lib64/libstdc++.so.6 (0x00007f69ff638000)
	libm.so.6 => /lib64/libm.so.6 (0x00007f69ff2a4000)
	libgcc_s.so.1 => /lib64/libgcc_s.so.1 (0x00007f69ff08c000)
	libc.so.6 => /lib64/libc.so.6 (0x00007f69feccd000)
	libdl.so.2 => /lib64/libdl.so.2 (0x00007f69feac9000)
	libpthread.so.0 => /lib64/libpthread.so.0 (0x00007f69fe8aa000)
	/lib64/ld-linux-x86-64.so.2 (0x00007f69ffe16000)


```

